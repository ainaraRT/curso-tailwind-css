module.exports = {
  content: [
    "./index.html",
    "./css/estilos.css"
  ],
  theme: {
    extend: {},
    fontFamily: {
      'sans': ['Poppins', 'Arial', 'sans-serif']
    }
  },
  plugins: [],
}
