module.exports = {
  content: [
    "./index.html",
    "./css/estilos.css"
  ],
  theme: {
    extend: {},
    fontFamily: {
      'sans': ['Poppins', 'Arial', 'sans-serif']
    }
  },
  variants: {
    borderWidth: ['responsive', 'hover', 'focus'],
  },
  plugins: [],
}
