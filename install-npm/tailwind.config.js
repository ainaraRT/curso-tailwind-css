module.exports = {
  content: [
    "./public/index.html",
    "./public/final.css"
  ],
  theme: {
    colors: {
      'danger': '#ff5f40',
      'info': {
        '900': '#234e52',
        '800': '#285e61',
      },
      'blue': '#1fb6ff',
      'purple': '#7e5bef',
      'pink': '#ff49db',
      'orange': '#ff7849',
      'green': '#13ce66',
      'yellow': '#ffc82c',
      'gray-dark': '#273444',
      'gray': '#8492a6',
      'gray-light': '#d3dce6',
    },
    fontFamily: {
      rale: ['Raleway']
    },
    extend: {
      spacing: {
        '8xl': '96rem',
        '9xl': '128rem',
      },
      borderRadius: {
        '4xl': '2rem',
      }
    }
  },
}
