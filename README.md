# Curso Tailwind CSS

Curso sobre cómo utilizar Tailwind CSS en [Udemy](https://www.udemy.com/course/tailwindcss/)


### [Tailwind Español](https://bluuweb.github.io/tailwindcss/)

### [Tailwind CSS](https://tailwindcss.com/docs/installation)

## NPM INSTALL
```shell
PS C:\Users\cursoTailwindCSS\curso-tailwind-css\install-npm> npm init -y
Wrote to C:\Users\cursoTailwindCSS\curso-tailwind-css\install-npm\package.json:

{
  "name": "curso-tailwind-css",
  "version": "1.0.0",
  "description": "Curso sobre cómo utilizar Tailwind CSS en [Udemy](https://www.udemy.com/course/tailwindcss/)",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "repository": {
    "type": "git",
    "url": "git+https://gitlab.com/ainaraRT/curso-tailwind-css.git"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "bugs": {
    "url": "https://gitlab.com/ainaraRT/curso-tailwind-css/issues"
  },
  "homepage": "https://gitlab.com/ainaraRT/curso-tailwind-css#readme"
}

PS C:\Users\cursoTailwindCSS\curso-tailwind-css\install-npm> npm i tailwindcss

added 60 packages, and audited 61 packages in 9s

12 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
npm notice 
npm notice New minor version of npm available! 8.3.1 -> 8.7.0
npm notice Changelog: https://github.com/npm/cli/releases/tag/v8.7.0
npm notice Run npm install -g npm@8.7.0 to update!
npm notice 

```

### Compilar
```shell
PS C:\Users\curso-tailwind-css\install-npm> npx tailwindcss build src/estilos.css -o public/final.css

```
Si necesitamos que funcione porque de la otra manera no se muestra:

```shell

PS C:\Users\curso-tailwind-css\install-npm> npx tailwindcss -i src/estilos.css -o public/final.css --watch  
[deprecation] Running tailwindcss without -i, please provide an input file.

warn - The `content` option in your Tailwind CSS configuration is missing or empty.
warn - Configure your content sources or your generated CSS will be missing styles.
warn - https://tailwindcss.com/docs/content-configuration

Done in 252ms.
```

### Personalizar
```shell
PS C:\Users\cursoTailwindCSS\curso-tailwind-css\install-npm> npx tailwindcss init       

Created Tailwind CSS config file: tailwind.config.js
```

### Dentro del archivo tailwind.config.js
Poner los archivos donde tienen que modificarse:

```json
content: [
    "./public/index.html",
    "./public/final.css"
  ]
```

## Animaciones

### Iniciar el proyecto

```shell
PS C:\Users\cursoTailwindCSS\curso-tailwind-css\navbar-animado> npm init -y
Wrote to C:\Users\cursoTailwindCSS\curso-tailwind-css\navbar-animado\package.json:

{
  "name": "navbar-animado",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "keywords": [],
  "author": "",
  "license": "ISC",
  "description": ""
}

C:\Users\cursoTailwindCSS\curso-tailwind-css\navbar-animado> npm i tailwindcss

added 60 packages, and audited 61 packages in 8s

12 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities

PS C:\Users\cursoTailwindCSS\curso-tailwind-css\navbar-animado> npx tailwindcss init

Created Tailwind CSS config file: tailwind.config.js

PS C:\Users\cursoTailwindCSS\curso-tailwind-css\navbar-animado> npx tailwindcss build src/tailwind.css -o css/estilos.css
[deprecation] Running tailwindcss without -i, please provide an input file.

warn - No utility classes were detected in your source files. If this is unexpected, double-check the `content` option in your Tailwind CSS configuration.
warn - https://tailwindcss.com/docs/content-configuration

Done in 362ms.
```

### Compilar
Si necesitamos que funcione porque de la otra manera no se muestra:

```shell

PS C:\Users\curso-tailwind-css\install-npm> npx tailwindcss -i src/tailwind.css -o css/estilos.css --watch  
[deprecation] Running tailwindcss without -i, please provide an input file.

warn - The `content` option in your Tailwind CSS configuration is missing or empty.
warn - Configure your content sources or your generated CSS will be missing styles.
warn - https://tailwindcss.com/docs/content-configuration

Done in 252ms.
```